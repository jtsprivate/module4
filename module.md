# Как находить корни квадратного трёхчлена?

Запишем квадратный трёхчлен в общем виде:

ax^2+bx+c,a\en0,

где a,b,c - фиксированные числовые значения.

Сразу скажем, что (действительных) корней может быть от нуля до двух. Если корень один, то иногда говорят, что найдено два совпадающих друг с другом корня.

Есть такое понятие, как дискриминант квадратного трёхчлена:

D=b^2-4ac.

По нему определяют, сколько корней у нашего трёхчлена:

- Если D>0, то корня два и оба они различны
- Если D<0, то (действительных) корней нет
- Если D=0, то корень один, кратности два (два одинаковых)

Теперь о том, как найти сами корни. Есть два принятых пути:

1. Через формулу
x_{1,2} = \frac{-b\pm\sqrt{D}}{2a}, D - дискриминант

2. Через теорему Виета.
Поделим всё уравнение на коффициент 'a', не равный по условию нулю и переобозначим переменные:
x^2+px+q=0, p=\frac{b}{a},q=\frac{c}{a}. Теперь пусть х_1, x_2 - корни. Тогда
x_1+x_2=-p,
x₁_1x_2=q
Решать такую систему 'в лоб' не рационально, так как приведёт вновь к квадратному уравнению. Такой способ эффективен, если можно нетрудно подобрать корни в голове.

Нередко корни в квадратных уравнениях целые, и второй способ в действительности может быстро дать вам корни без лишних записей, но первый способ является универсальным.

После находения корней квадратный трёхчлен можно записать в виде

- a(x - x_1)(x - x_2), если корня два
- a(x - x_{1,2})^2, если два корня совпадают

Если (дейсвтительных) корней нету, то квадратный трёхчлен 'собрать в скобки' нельзя.

Если действительных корней может не существовать, то разложение на комплексные корни всегда существует, и находятся они по аналогии с первой формулой, в которой в таком случае получится квадратный корень из отрицательного числа. Если вам нужно уметь решать квадратное уравнение в комплексных числах, то ознакомьтесь с темой \mref{"нахождение комплексных корней в многочленах"}.